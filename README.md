# README #
Android Project Team 9

### What is this repository for? ###
We are making a sound app which activities include a soundboard, drums, and later on another instrument.
App designed to be ran on an Android Studio Emulator Device - Google Nexus 5 - 1080x1920 xxhdpi - 4.4.4 - API 19
If there is an error with playing the sound, then wipe the data on the emulator instance and cold boot it.

### How do I get set up? ###
On startup, the menu is launched and at the moment gives three options; soundboard, drums, or about page.
Users get back to the menu by pressing the back button.
Style is created, basic functionality has been implemented
Tough features to implement may be; gestures instead of plain buttons, connecting paths to local files,
and we may need a media player or audio media interface.

### Contribution guidelines ###
The app has been a great group effort and we help each other whenever it's needed but for the most part;
Hayden Rainey is creating the menu, drums, and about page
Dylan Eckstein is creating the soundboard and handling the repository

### Who do I talk to? ###
Dylan Eckstein is the Repo owner

### What is included in the app
Main Menu with buttons leading to the soundboard, drums, and about activities.
Soundboard: A gridview of buttons(really textviews) that play a sound when pressed. If the user holds the button down,
a dialog appears letting them change the button's text, background color, and text color.
Drums: A constraint layout of a virtual drumset letting the user play the drums with their fingers. It is a lot more 
fun when playing on a phone instead of using a mouse with the emulator.
About: A small informative activity with an app summary and a list of our sources.