package team9.team9androidproject;

import android.app.Dialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class drums extends AppCompatActivity {

   //tracks contains all the sounds for the drum

    private MediaPlayer mediaPlayer;
    public int[] tracks = new int[8];
    public int[] buttons = new int[8];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drums);


        //instrument sounds are stored in the tracks array
       tracks[0] = R.raw.sym1_sound;
       tracks[1] = R.raw.sym1_sound;
       tracks[2] = R.raw.tom1_sound;
       tracks[3] = R.raw.tom2_sound;
       tracks[4] = R.raw.tom3_sound;
       tracks[5] = R.raw.hat1_sound;
       tracks[6] = R.raw.hat2_sound;
       tracks[7] = R.raw.snare_sound;


       buttons[0] = R.id.ins_0;
       buttons[1] = R.id.ins_1;
       buttons[2] = R.id.ins_2;
       buttons[3]= R.id.ins_3;
       buttons[4]= R.id.ins_4;
       buttons[5] = R.id.ins_5;
       buttons[6] = R.id.ins_6;
       buttons[7] = R.id.ins_7;


       //onTouch used instead of onClick to decrease latency on button presses
        for(int i = 0; i < buttons.length;i++)
        {
            Button btn = findViewById(buttons[i]);
            btn.setOnTouchListener(new View.OnTouchListener(){
                public boolean onTouch(View view, MotionEvent event)
                {
                    if(event.getAction() == MotionEvent.ACTION_UP)
                    {
                        switch(view.getId())
                        {
                            case R.id.ins_0:
                                mediaPlayer = MediaPlayer.create(getApplicationContext(),tracks[0]);
                                break;
                            case R.id.ins_1:
                                mediaPlayer = MediaPlayer.create(getApplicationContext(),tracks[1]);
                                break;
                            case R.id.ins_2:
                                mediaPlayer = MediaPlayer.create(getApplicationContext(),tracks[2]);
                                break;
                            case R.id.ins_3:
                                mediaPlayer = MediaPlayer.create(getApplicationContext(),tracks[3]);
                                break;
                            case R.id.ins_4:
                                mediaPlayer = MediaPlayer.create(getApplicationContext(),tracks[4]);
                                break;
                            case R.id.ins_5:
                                mediaPlayer = MediaPlayer.create(getApplicationContext(),tracks[5]);
                                break;
                            case R.id.ins_6:
                                mediaPlayer = MediaPlayer.create(getApplicationContext(),tracks[6]);
                                break;
                            case R.id.ins_7:
                                mediaPlayer = MediaPlayer.create(getApplicationContext(),tracks[7]);
                                break;
                        }
                        mediaPlayer.start();
                        //on completion listener set to reset and release the media player after a sound has been played
                        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mediaPlayer) {
                                mediaPlayer.reset();
                                mediaPlayer.release();
                                mediaPlayer = null;
                            }
                        });
                        return true;
                    }
                    return false;
                }
            });


        }


    }





}
