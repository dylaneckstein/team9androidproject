package team9.team9androidproject;
//Created by Dylan Eckstein Spring 2018 for Android class

import android.graphics.drawable.GradientDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;
import java.util.ArrayList;

 /* Created by s525901-Dylan Eckstein on 2/16/2018.
 * YOU MUST USE THE ANDROID STUDIO'S EMULATOR FOR OPTIMAL SOUND QUALITY, NOT GENYMOTION.
 * This runs on Nexus 5 xxhdpi API 19 4.4 KitKat android studio emulator
 * This activity uses an ArrayAdapter to create a GridList. Each TextView in the GridView acts
 * like a button, and onItemClick they play a certain media file located in the res/raw folder.
 * Since it is an adapter, we have the ability to scroll.
 * If the user holds down on a button an edit dialog pops us and the user can change the text,
 * background color, and text color of the button.
 */

public class soundboard extends AppCompatActivity implements editDialog.EditorRef{

    public ArrayList<String> buttonArray = new ArrayList();     //list of button texts
    static MediaPlayer mp = new MediaPlayer();      //the media player we'll be using
    int btnID;     //used for the change button settings dialog

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.soundboard);
        //fill arrays
        final int[] musicArr = {R.raw.bluelazer, R.raw.bluetrees, R.raw.imblue, R.raw.impressive,
                R.raw.thunder, R.raw.thunderstruck, R.raw.abadu, R.raw.clickboom, R.raw.freewill,
                R.raw.initiald, R.raw.maria, R.raw.oceanman, R.raw.scatman, R.raw.tokyo,
                R.raw.whistle, R.raw.yuhyuh};
        buttonArray.add("Laser");
        buttonArray.add("Blue Trees");
        buttonArray.add("I'm blue");
        buttonArray.add("Impressive");
        buttonArray.add("Thunder");
        buttonArray.add("ThunderStruck");
        buttonArray.add("Trumpets");
        buttonArray.add("Click Click Boom");
        buttonArray.add("Freewill");
        buttonArray.add("Initial D");
        buttonArray.add("Maria");
        buttonArray.add("Ocean Man");
        buttonArray.add("Scatman");
        buttonArray.add("Tokyo Drift");
        buttonArray.add("Whistle");
        buttonArray.add("Yeah");
        //create adapter and set gridview to it
        ArrayAdapter buttonList = new soundboardAdapter(this,
                R.layout.sound_button, R.id.text1, buttonArray);
        final GridView theGrid = (GridView) findViewById(R.id.buttonList);
        theGrid.setAdapter(buttonList);

        //on item click
        theGrid.setOnItemClickListener(
            new AdapterView.OnItemClickListener() {
                public void onItemClick( AdapterView<?> parent, View view,
                                         int position, long id) {
                    //the position decides which file is played
                    mp = MediaPlayer.create(getApplicationContext(), musicArr[position]);
                    mp.start();
                    mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        public void onCompletion(MediaPlayer mp) {
                            mp.release();
                        }
                    });
                }
            });

        //on long item click
        theGrid.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    public boolean onItemLongClick( AdapterView<?> parent, View view,
                                             int position, long id) {
                        //records which button was pressed and opens up the dialog
                        btnID = position;
                        editDialog editDF = new editDialog();
                        editDF.show(getSupportFragmentManager(),"TAG");
                        refreshGrid();
                        return true;
                    }
                });
        refreshGrid();
    }

    //this function changes the text of the button
    //input is a string
    @Override
    public void changeBTNText(String str) {
        if(str != null && str != ""){
            buttonArray.set(btnID, str);
        }
        refreshGrid();
    }

    //this function changes the text color of the button
    //input is a string and is parsed into hex
    @Override
    public void changeBTNColor(String str) {
        if(str != null && str != ""){
            //first get the view we are changing
            GridView theGrid = findViewById(R.id.buttonList);
            View btn = getViewByPosition(btnID, theGrid);
            TextView tv = btn.findViewById(R.id.text1);
            GradientDrawable drawable = (GradientDrawable) tv.getBackground();
            int color = 0;
            //didn't want to have to do it like this but the function I had in mind deprecated
            //here we get the hex value from colors.xml
            switch (str){
                case "Black":
                    color = ContextCompat.getColor(this, R.color.Black);
                    break;
                case "White":
                    color = ContextCompat.getColor(this, R.color.White);
                    break;
                case "Gray":
                    color = ContextCompat.getColor(this, R.color.Gray);
                    break;
                case "Red":
                    color = ContextCompat.getColor(this, R.color.Red);
                    break;
                case "Blue":
                    color = ContextCompat.getColor(this, R.color.Blue);
                    break;
                case "Green":
                    color = ContextCompat.getColor(this, R.color.Green);
                    break;
                case "Yellow":
                    color = ContextCompat.getColor(this, R.color.Yellow);
                    break;
                case "Orange":
                    color = ContextCompat.getColor(this, R.color.Orange);
                    break;
                case "Purple":
                    color = ContextCompat.getColor(this, R.color.Purple);
                    break;
            }
            //set the color and refresh the view
            drawable.setColor(color);
        }
        refreshGrid();
    }

    //this function changes the text color of the button
    //input is a string and is parsed into hex
    @Override
    public void changeTXTColor(String str) {
        if(str != null && str != ""){
            //first get the view we are changing
            GridView theGrid = findViewById(R.id.buttonList);
            View btn = getViewByPosition(btnID, theGrid);
            TextView tv = btn.findViewById(R.id.text1);
            int color = 0;
            //didn't want to have to do it like this but the function I had in mind deprecated
            //here we get the hex value from colors.xml
            switch (str){
                case "Black":
                    color = ContextCompat.getColor(this, R.color.Black);
                    break;
                case "White":
                    color = ContextCompat.getColor(this, R.color.White);
                    break;
                case "Gray":
                    color = ContextCompat.getColor(this, R.color.Gray);
                    break;
                case "Red":
                    color = ContextCompat.getColor(this, R.color.Red);
                    break;
                case "Blue":
                    color = ContextCompat.getColor(this, R.color.Blue);
                    break;
                case "Green":
                    color = ContextCompat.getColor(this, R.color.Green);
                    break;
                case "Yellow":
                    color = ContextCompat.getColor(this, R.color.Yellow);
                    break;
                case "Orange":
                    color = ContextCompat.getColor(this, R.color.Orange);
                    break;
                case "Purple":
                    color = ContextCompat.getColor(this, R.color.Purple);
                    break;
            }
            //set the color and refresh the view
            tv.setTextColor(color);
        }
        refreshGrid();
    }

    //this function returns a particular view our of the grid view
    //inputs are the position and the gridview
    public View getViewByPosition(int pos, GridView gridView) {
        final int firstListItemPosition = gridView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + gridView.getChildCount() - 1;
        if (pos < firstListItemPosition || pos > lastListItemPosition ) {
            return gridView.getAdapter().getView(pos, null, gridView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return gridView.getChildAt(childIndex);
        }
    }

    //refreshes the view
    public void refreshGrid(){
        GridView theGrid = findViewById(R.id.buttonList);
        theGrid.invalidateViews();
    }

}