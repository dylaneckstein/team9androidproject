package team9.team9androidproject;

/*
Created by Dylan Eckstein and Hunter Rainey on Feb 15, 2018
The app currently has limited functionality and only switched between the activites
We have taken out the App label
More comments will come with added functionality
Designed to be ran on a Genymotion Google Nexus 5 - 4.4.4 - API 19 - 1080x1920
 */
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    //on button press start the drums activity
    public void drums(View v){
        Intent drums = new Intent(this, drums.class);
        startActivity(drums);
    }

    //on button press start the soundboard activity
    public void soundboard(View w){
        Intent soundboard = new Intent(this, soundboard.class);
        startActivity(soundboard);
    }

    //on button press start the about activity
    public void about(View x){
        Intent about = new Intent(this, about.class);
        startActivity(about);
    }

}
