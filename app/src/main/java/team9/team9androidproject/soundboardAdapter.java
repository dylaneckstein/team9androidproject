package team9.team9androidproject;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;

/**
 * Created by s525901-Dylan Eckstein on 3/7/2018.
 * Simple adapter, the resource we use is sound_button.xml which is just a TextView
 * Before this I tried making custom list items with different Buttons and TextViews, but
 * it became too complicated and didn't work.
 * Why have Buttons when TextViews act like Buttons in an ArrayAdapter?
 */

class soundboardAdapter extends ArrayAdapter<String>{
    public soundboardAdapter(Context context, int resource, int textViewID, List<String>data){
        super(context,resource,textViewID,data);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        TextView text1 = view.findViewById(R.id.text1);
        text1.setText(getItem(position));

        return view;
    }

}


