package team9.team9androidproject;
//Created by Dylan Eckstein Spring 2018 for Android class

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class editDialog extends DialogFragment {
    //context reference
    EditorRef editor;

    //our interface
    public interface EditorRef{
        void changeBTNText(String str);
        void changeBTNColor(String str);
        void changeTXTColor(String str);
    }

    public void onAttach(Activity act){
        super.onAttach(act);
        editor = (EditorRef)act;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View v = inflater.inflate(R.layout.edit_button, null);

        //this fills the spinners with the options
        String[] stringArr = {"Black", "White", "Gray", "Red", "Purple", "Blue", "Green", "Yellow", "Orange"};
        Spinner ccolor = (Spinner) v.findViewById(R.id.color2change);
        Spinner tcolor = (Spinner) v.findViewById(R.id.textcolor2change);
        ArrayAdapter<String> adapter = new ArrayAdapter<>((Context) editor,
                android.R.layout.simple_spinner_item, stringArr);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ccolor.setAdapter(adapter);
        tcolor.setAdapter(adapter);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(v)
                .setTitle("Change Button Settings")
                .setPositiveButton(R.string.change, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Dialog d = (Dialog)dialog;
                        //this gets the values from the inputs and sends them to the activity
                        EditText etext = d.findViewById(R.id.text2change);
                        Spinner scolor = d.findViewById(R.id.color2change);
                        Spinner stxtcolor = d.findViewById(R.id.textcolor2change);
                        String ctext = etext.getText().toString();
                        String ccolor = scolor.getSelectedItem().toString();
                        String txtcolor = stxtcolor.getSelectedItem().toString();
                        Log.d("here1", ccolor + " " + txtcolor);
                        //error checks and passes value to function
                        if (ctext != null){
                            editor.changeBTNText(ctext);
                        }
                        if (ccolor != null){
                            editor.changeBTNColor(ccolor);
                        }
                        if (txtcolor != null){
                            editor.changeTXTColor(txtcolor);
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //nothing
                    }
                });
        return builder.create();
    }




}
